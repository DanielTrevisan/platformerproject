﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour {

	public void LoadNewLevel(){
		print("Loading.....");
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void SaveLevel(){
		print("Saving.....");
		GameManager.instance.SaveScene();
	}

	public void loadByIndex(int index){
		SceneManager.LoadScene (index);
	}
}
