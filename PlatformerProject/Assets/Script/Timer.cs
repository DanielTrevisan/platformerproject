﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	public Text timerLabel;

	public float time;
	public bool stopTime = true;

	void Start(){
		timerLabel = GameObject.Find ("Timer").GetComponent<Text>();
	}


	public void startTimer(){
		stopTime = false;
	}

	void Update() {
		if (stopTime != true) {
			time += Time.deltaTime;

			string minutes = Mathf.Floor(time / 60).ToString("00");
			string seconds = (time % 60).ToString("00");
			if (seconds == "60") {
				seconds = "00";
				minutes = Mathf.Ceil(time / 60).ToString("00");

			}
			//var fraction = (time * 100) % 100;

			//update the label value
			timerLabel.text = string.Format ("Time: " + minutes + ":" +  seconds);
			if (time > GameManager.instance.MaxTime) {
				timerLabel.color = new Color (1, 0, 0);
			}
		}
	}

	public void stopCounting(){
		stopTime = true;
		print (timerLabel.text);
	}

	public void resetTimer(){
		time = 0f;
		stopTime = true;
		timerLabel.color = new Color (0, 0, 0);

	}
}