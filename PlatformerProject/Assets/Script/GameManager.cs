﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class LevelModel{
	public float timeToFinish;
	public WallStruct[] walls;
	public WallStruct[] movingWalls;
	public WallStruct[] platforms;
	public DeathZoneStruct[] deathzones;
	public DeathZoneStruct[] spikes;
	public CollectableStruct[] collectables;
	public PlayerStruct player;
	public FinishZoneStruct finishZone;
}

[Serializable]
public struct DeathZoneStruct{
	public Vector3Struct position;
	public QuaternionStruct rotation;
	public Vector3Struct scale;
}

[Serializable]
public struct FinishZoneStruct{
	public Vector3Struct position;
	public QuaternionStruct rotation;
	public Vector3Struct scale;
}

[Serializable]
public struct CollectableStruct{
	public Vector3Struct position;
	public QuaternionStruct rotation;
	public Vector3Struct scale;
}

[Serializable]
public struct WallStruct{
	public Vector3Struct position;
	public QuaternionStruct rotation;
	public Vector3Struct scale;
	public bool moving;
	public movingPlatformStruct mPS;
}

[Serializable]
public struct movingPlatformStruct{
	public Vector3Struct []LocalWaypoints;
	public float speed;
	public bool cyclic;
	public float waitTime;
	public float easeAmount;
}

[Serializable]
public struct PlayerStruct{
	public Vector3Struct position;
	public QuaternionStruct rotation;
	public Vector3Struct scale;

	public float maxSlopeAngle;
	public float maxJumpHeight;
	public float minJumpHeight;
	public float timeToJumpApex;
	public Vector2Struct wallJumpClimb;
	public Vector2Struct wallJumpOff;
	public Vector2Struct wallLeap;
	public float wallSlideSpeedMax;
	public float wallStickTime;

	public void fromPlayer(GameObject player){
		Player plScript = player.GetComponent<Player>();
		position.FromVec3(player.transform.position);
		rotation.FromQuaternion(player.transform.rotation);
		scale.FromVec3(player.transform.localScale);
		maxSlopeAngle = player.GetComponent<Controller2D>().maxSlopeAngle;

		maxJumpHeight = plScript.maxJumpHeight;
		minJumpHeight = plScript.minJumpHeight;
		timeToJumpApex = plScript.timeToJumpApex;
		wallJumpClimb.FromVec2(plScript.wallJumpClimb);
		wallJumpOff.FromVec2(plScript.wallJumpOff);
		wallLeap.FromVec2(plScript.wallLeap);
		wallSlideSpeedMax = plScript.wallSlideSpeed;
		wallStickTime = plScript.wallStickTime;
	}
}

[Serializable]
public struct QuaternionStruct {
	public float x;
	public float y;
	public float z;
	public float w;

	public Quaternion ToQuaternion(){
		return new Quaternion(x,y,z,w);
	}

	public void FromQuaternion(Quaternion entry){
		x = entry.x;
		y = entry.y;
		z = entry.z;
		w = entry.w;
	}
}

[Serializable]
public struct Vector3Struct {
	public float x;
	public float y;
	public float z;
//	public float mag;
//	public float sqrtMag;

	public void FromVec3(Vector3 entry){
//		mag = entry.magnitude;
//		sqrtMag = entry.sqrMagnitude;
		x = entry.x;
		y = entry.y;
		z = entry.z;
	}

	public Vector3 ToVec3(){
		Vector3 a = new Vector3(x,y,z);
		return a;
	}
}

[Serializable]
public struct Vector2Struct{
	public float x;
	public float y;

	public void FromVec2(Vector2 entry){
		x = entry.x;
		y = entry.y;
	}

	public Vector2 ToVec2(){
		return new Vector2(x,y);
	}
}

public class GameManager : MonoBehaviour {

	public GameObject prefabWall;
	public GameObject prefabPlatform;
	public GameObject prefabMovingWall;
	public GameObject prefabDeathZone;
	public GameObject prefabExit;
	public GameObject prefabPlayer;
	public GameObject prefabCamera;
	public GameObject prefabCollectable;

	public AudioClip coinCollect;
	public AudioClip levelCompleteSound;
	public AudioClip teleportSound;


	bool setup, coins = false;

	public static GameManager instance = null;
	LevelModel model;

	[HideInInspector]
	public List<GameObject> coinsArray = new List<GameObject>();

	public int MaxTime;

	public int Level(){
		return SceneManager.GetActiveScene().buildIndex-1;
	}

	void Awake()
	{
		//Check if instance already exists
		if (instance == null)

			//if not, set instance to this
			instance = this;

		//If instance already exists and it's not this:
		else if (instance != this )

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);	

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);

//		instance.setup = false;

		this.GetComponent<Timer> ().startTimer();

	}
	// Use this for initialization
	void Start () {
	
	}

	public void SaveScene(){
		LevelModel model = new LevelModel();

		WallStruct[] toSaveWall = null;
		GameObject[] wallsGO = null;

		GameObject[] platformGO = null;
		WallStruct[] toSavePlatform = null;

		GameObject[] movingWallsGO = null;
		WallStruct[] toSaveMovingWall = null;

		DeathZoneStruct[] toSaveDeath = null;
		GameObject[] deathZoneGO = null;

		DeathZoneStruct[] toSaveSpike = null;
		GameObject[] spikeGO = null;

		CollectableStruct[] toSaveCollectables = null;
		GameObject[] collectableGO = null;

		if(GameObject.FindGameObjectsWithTag("Wall").Length != 0){
			wallsGO = GameObject.FindGameObjectsWithTag("Wall");
			toSaveWall = new WallStruct[wallsGO.Length];
		}

		if(GameObject.FindGameObjectsWithTag("Through").Length != 0){
			platformGO = GameObject.FindGameObjectsWithTag("Through");
			toSavePlatform = new WallStruct[platformGO.Length];
		}

		if(GameObject.FindGameObjectsWithTag("MovingPlat").Length != 0){
			movingWallsGO = GameObject.FindGameObjectsWithTag("MovingPlat");
			toSaveMovingWall = new WallStruct[movingWallsGO.Length];
		}
		if(GameObject.FindGameObjectsWithTag("DeathZone").Length != 0){
			deathZoneGO = GameObject.FindGameObjectsWithTag("DeathZone");
			toSaveDeath = new DeathZoneStruct[deathZoneGO.Length];
		}

		if(GameObject.FindGameObjectsWithTag("Spike").Length != 0){
			spikeGO = GameObject.FindGameObjectsWithTag("Spike");
			toSaveSpike = new DeathZoneStruct[spikeGO.Length];
		}

		if(GameObject.FindGameObjectsWithTag("Collectable").Length != 0){
			collectableGO = GameObject.FindGameObjectsWithTag("Collectable");
			toSaveCollectables = new CollectableStruct[collectableGO.Length];
		}

		GameObject exit = GameObject.FindGameObjectWithTag("FinishZone");

		GameObject player = GameObject.FindGameObjectWithTag("Player");

		model.timeToFinish = GameManager.instance.GetComponent<Timer>().time;

		//Start Saving Walls ..........
		if(wallsGO != null)
		for(int i = 0; i < wallsGO.Length; i++){
			WallStruct a = new WallStruct();

			//Position of a wall
			a.position.FromVec3(wallsGO[i].transform.position);
			a.rotation.FromQuaternion(wallsGO[i].transform.rotation);
			a.scale.FromVec3(wallsGO[i].transform.localScale);
			if(wallsGO[i].GetComponent<PlatformerController>() != null){
				PlatformerController platform = wallsGO[i].GetComponent<PlatformerController>();
				a.moving = true;
				a.mPS.LocalWaypoints = new Vector3Struct[platform.localWaypoints.Length];
				for(int j=0;j<platform.localWaypoints.Length;j++){
					a.mPS.LocalWaypoints[j].FromVec3(platform.localWaypoints[j]);
				}
				a.mPS.speed = platform.speed;
				a.mPS.cyclic = platform.cyclic;
				a.mPS.waitTime = platform.waitTime;
				a.mPS.easeAmount = platform.easeAmount;

			}else{
				a.moving = false;
			}

			toSaveWall[i] = a;
		}
			
		model.walls = toSaveWall;
		//End Saving Walls ..........

		//Start Saving ThroughPlatforms ..........
		if(platformGO != null)
			for(int i = 0; i < platformGO.Length; i++){
				WallStruct a = new WallStruct();

				//Position of a wall
				a.position.FromVec3(platformGO[i].transform.position);
				a.rotation.FromQuaternion(platformGO[i].transform.rotation);
				a.scale.FromVec3(platformGO[i].transform.localScale);
				if(platformGO[i].GetComponent<PlatformerController>() != null){
					if(platformGO[i].GetComponent<PlatformerController>().localWaypoints.Length>0){
						a.moving = false;
					}else{
					PlatformerController platform = platformGO[i].GetComponent<PlatformerController>();
					a.moving = true;
					a.mPS.LocalWaypoints = new Vector3Struct[platform.localWaypoints.Length];
					for(int j=0;j<platform.localWaypoints.Length;j++){
						a.mPS.LocalWaypoints[j].FromVec3(platform.localWaypoints[j]);
					}
					a.mPS.speed = platform.speed;
					a.mPS.cyclic = platform.cyclic;
					a.mPS.waitTime = platform.waitTime;
					a.mPS.easeAmount = platform.easeAmount;
					}

				}else{
					a.moving = false;
				}

				toSavePlatform[i] = a;
			}

		model.platforms = toSavePlatform;
		//End Saving ThroughPlatforms ..........

		//Start Saving MovingPlatforms ..........
		if(movingWallsGO != null)
			for(int i = 0; i < movingWallsGO.Length; i++){
				WallStruct a = new WallStruct();

			//Position of a wall
				a.position.FromVec3(movingWallsGO[i].transform.position);
				a.rotation.FromQuaternion(movingWallsGO[i].transform.rotation);
				a.scale.FromVec3(movingWallsGO[i].transform.localScale);
				if(movingWallsGO[i].GetComponent<PlatformerController>() != null){
					if(movingWallsGO[i].GetComponent<PlatformerController>().localWaypoints.Length==0){
						a.moving = false;
					}else{
						PlatformerController platform = movingWallsGO[i].GetComponent<PlatformerController>();
						a.moving = true;
						a.mPS.LocalWaypoints = new Vector3Struct[platform.localWaypoints.Length];
						for(int j=0;j<platform.localWaypoints.Length;j++){
							a.mPS.LocalWaypoints[j].FromVec3(platform.localWaypoints[j]);
						}
						a.mPS.speed = platform.speed;
						a.mPS.cyclic = platform.cyclic;
						a.mPS.waitTime = platform.waitTime;
						a.mPS.easeAmount = platform.easeAmount;
					}

				}else{
					a.moving = false;
				}

			toSaveMovingWall[i] = a;
		}

		model.movingWalls = toSaveMovingWall;
		//End Saving MovingPlatforms ..........

		//Start Saving DeathZones ..........
		if(deathZoneGO != null)
		for(int i = 0; i < deathZoneGO.Length; i++){
			DeathZoneStruct a = new DeathZoneStruct();
			a.position.FromVec3(deathZoneGO[i].transform.position);
			a.rotation.FromQuaternion(deathZoneGO[i].transform.rotation);
			a.scale.FromVec3(deathZoneGO[i].transform.localScale);

			toSaveDeath[i] = a;
		}

		model.deathzones = toSaveDeath;
		//End Saving DeathZones ..........

		//Start Saving Collectables ..........
		if(collectableGO != null)
			for(int i = 0; i < collectableGO.Length; i++){
				CollectableStruct a = new CollectableStruct();
				a.position.FromVec3(collectableGO[i].transform.position);
				a.rotation.FromQuaternion(collectableGO[i].transform.rotation);
				a.scale.FromVec3(collectableGO[i].transform.localScale);

				toSaveCollectables[i] = a;
			}

		model.collectables = toSaveCollectables;
		//End Saving Collectables ..........

		//Start Saving Spikes ..........
		if(spikeGO != null)
			for(int i = 0; i < spikeGO.Length; i++){
				DeathZoneStruct a = new DeathZoneStruct();
				a.position.FromVec3(spikeGO[i].transform.position);
				a.rotation.FromQuaternion(spikeGO[i].transform.rotation);
				a.scale.FromVec3(spikeGO[i].transform.localScale);

				toSaveSpike[i] = a;
			}

		model.spikes = toSaveSpike;
		//End Saving Spikes ..........

		//Start Saving FinishZone ..........
		FinishZoneStruct finish = new FinishZoneStruct();

		finish.position.FromVec3(exit.transform.position);
		finish.rotation.FromQuaternion(exit.transform.rotation);
		finish.scale.FromVec3(exit.transform.localScale);

		model.finishZone = finish;
		//End Saving FinishZone ..........

		//Start Saving Player ..........
		PlayerStruct pl = new PlayerStruct();
		pl.fromPlayer(player);

		model.player = pl;
		//End Saving Player ..........

		string json = JsonUtility.ToJson(model);

		print(json);

		System.IO.File.WriteAllText("./Assets/Levels/Level"+ GameManager.instance.Level() +".json", json);

	}

	public void loadGameLevel(int number){

		if(instance.setup == false){
			instance.setup = true;

			string file = System.IO.File.ReadAllText("./Assets/Levels/Level"+ number +".json");
			print(file);
			LevelModel level = JsonUtility.FromJson<LevelModel>(file);

			GameManager.instance.GetComponent<Timer>().time = level.timeToFinish;

			//Creating Player...........
			if(prefabPlayer != null){
				GameObject playerGO = Instantiate(prefabPlayer);
				playerGO.transform.position = level.player.position.ToVec3();
				playerGO.transform.rotation = level.player.rotation.ToQuaternion();
				playerGO.transform.localScale = level.player.scale.ToVec3();

				playerGO.GetComponent<Controller2D>().maxSlopeAngle = level.player.maxSlopeAngle;

				Player playerComponent = playerGO.GetComponent<Player>();
				playerComponent.maxJumpHeight = level.player.maxJumpHeight;
				playerComponent.minJumpHeight = level.player.minJumpHeight;
				playerComponent.timeToJumpApex = level.player.timeToJumpApex;
				playerComponent.wallJumpClimb = level.player.wallJumpClimb.ToVec2();
				playerComponent.wallJumpOff = level.player.wallJumpOff.ToVec2();
				playerComponent.wallLeap = level.player.wallLeap.ToVec2();
				playerComponent.wallSlideSpeed = level.player.wallSlideSpeedMax;
				playerComponent.wallStickTime = level.player.wallStickTime;

				if(prefabCamera != null){
					GameObject camera = Instantiate(prefabCamera);
					camera.GetComponent<CameraFollow>().target = playerGO.GetComponent<Controller2D>();
				}

			}
			//..........End Player;

			//Creating Walls...........
			if(prefabWall != null){
				if(level.walls != null)
					foreach(WallStruct wall in level.walls){
						GameObject wallGO = Instantiate(prefabWall);
						wallGO.transform.position = wall.position.ToVec3();
						wallGO.transform.rotation = wall.rotation.ToQuaternion();
						wallGO.transform.localScale = wall.scale.ToVec3();
						if(wall.moving){
							PlatformerController plController = wallGO.GetComponent<PlatformerController>();
							plController.localWaypoints = new Vector3[wall.mPS.LocalWaypoints.Length];
							for(int i = 0; i< wall.mPS.LocalWaypoints.Length; i++){
								Vector3Struct a = wall.mPS.LocalWaypoints[i];
								plController.localWaypoints[i] = new Vector3(a.x,a.y,a.z);
							}
							plController.speed = wall.mPS.speed;
							plController.cyclic = wall.mPS.cyclic;
							plController.waitTime = wall.mPS.waitTime;
							plController.easeAmount = wall.mPS.easeAmount;
						}
					}
			}
			//..........End Walls;

			//Creating Moving Walls...........
			if(prefabMovingWall != null){
				if(level.movingWalls != null)
					foreach(WallStruct wall in level.movingWalls){
						GameObject wallGO = Instantiate(prefabMovingWall);
						wallGO.transform.position = wall.position.ToVec3();
						wallGO.transform.rotation = wall.rotation.ToQuaternion();
						wallGO.transform.localScale = wall.scale.ToVec3();
						if(wall.moving){
							PlatformerController plController = wallGO.GetComponent<PlatformerController>();
							plController.localWaypoints = new Vector3[wall.mPS.LocalWaypoints.Length];
							for(int i = 0; i< wall.mPS.LocalWaypoints.Length; i++){
								Vector3Struct a = wall.mPS.LocalWaypoints[i];
								plController.localWaypoints[i] = new Vector3(a.x,a.y,a.z);
							}
							plController.speed = wall.mPS.speed;
							plController.cyclic = wall.mPS.cyclic;
							plController.waitTime = wall.mPS.waitTime;
							plController.easeAmount = wall.mPS.easeAmount;
						}
					}
			}
			//..........End Moving Walls;

			//Creating Platforms...........
			if(prefabPlatform != null){
				if(level.platforms != null)
					foreach(WallStruct wall in level.platforms){
						GameObject wallGO = Instantiate(prefabPlatform);
						wallGO.transform.position = wall.position.ToVec3();
						wallGO.transform.rotation = wall.rotation.ToQuaternion();
						wallGO.transform.localScale = wall.scale.ToVec3();
						if(wall.moving){
							PlatformerController plController = wallGO.GetComponent<PlatformerController>();
							plController.localWaypoints = new Vector3[wall.mPS.LocalWaypoints.Length];
							for(int i = 0; i< wall.mPS.LocalWaypoints.Length; i++){
								Vector3Struct a = wall.mPS.LocalWaypoints[i];
								plController.localWaypoints[i] = new Vector3(a.x,a.y,a.z);
							}
							plController.speed = wall.mPS.speed;
							plController.cyclic = wall.mPS.cyclic;
							plController.waitTime = wall.mPS.waitTime;
							plController.easeAmount = wall.mPS.easeAmount;
						}
					}
			}
			//..........End Platform;

			//Creating Death Zones...........
			if(prefabDeathZone != null){
				if(level.deathzones != null)
					foreach(DeathZoneStruct deathZone in level.deathzones){
						GameObject deathZoneGO = Instantiate(prefabDeathZone);
						deathZoneGO.transform.position = deathZone.position.ToVec3();
						deathZoneGO.transform.rotation = deathZone.rotation.ToQuaternion();
						deathZoneGO.transform.localScale = deathZone.scale.ToVec3();
					}
			}
			//..........End Death Zones;

			//Creating Collectables...........
			if(prefabCollectable != null){
				if(level.collectables != null)
					foreach(CollectableStruct collectable in level.collectables){
						GameObject collectableGO = Instantiate(prefabCollectable);
						collectableGO.transform.position = collectable.position.ToVec3();
						collectableGO.transform.rotation = collectable.rotation.ToQuaternion();
						collectableGO.transform.localScale = collectable.scale.ToVec3();
					}
			}
			//..........End Collectables;

			//Creating Finish Zone...........
			if(prefabExit != null){
				GameObject finishGO = Instantiate(prefabExit);
				finishGO.transform.position = level.finishZone.position.ToVec3();
				finishGO.transform.rotation = level.finishZone.rotation.ToQuaternion();
				finishGO.transform.localScale = level.finishZone.scale.ToVec3();
			}
			//..........End Finish Zone;

			print("Finish Load!!!");

			instance.setup = false;
		}

	}

	public bool isSetup(){
		return setup;
	}
	public void pauseGame(){
		GameObject Player = GameObject.FindGameObjectWithTag ("Player");
		Player playerScript = Player.GetComponent<Player> ();
		playerScript.StopMovement = true;

		GameObject[] platforms = GameObject.FindGameObjectsWithTag ("MovingPlat");
		foreach (GameObject platform in platforms){
			PlatformerController platScript = platform.GetComponent<PlatformerController> ();
			platScript.StopMovement = true;
		}

		GameObject[] deathZones = GameObject.FindGameObjectsWithTag ("DeathZone");
		foreach (GameObject deathZone in deathZones){
			PlatformerController platScript = deathZone.GetComponent<PlatformerController> ();
			if (platScript != null){
				platScript.StopMovement = true;
			}
		}

		Timer timer = GameManager.instance.GetComponent<Timer> ();
		timer.stopTime = true;
	}
	public void unpauseGame(){
		GameObject Player = GameObject.FindGameObjectWithTag ("Player");
		Player playerScript = Player.GetComponent<Player> ();
		playerScript.StopMovement = false;

		GameObject[] platforms = GameObject.FindGameObjectsWithTag ("MovingPlat");
		foreach (GameObject platform in platforms){
			PlatformerController platScript = platform.GetComponent<PlatformerController> ();
			platScript.StopMovement = false;
		}
		GameObject[] deathZones = GameObject.FindGameObjectsWithTag ("DeathZone");
		foreach (GameObject deathZone in deathZones){
			PlatformerController platScript = deathZone.GetComponent<PlatformerController> ();
			platScript.StopMovement = false;
		}
		Timer timer = GameManager.instance.GetComponent<Timer> ();
		timer.stopTime = false;
	}

	public void moedaStuff(){
		//print(GameManager.instance.coinsArray.Count);
		if (coinsArray.Count == 1) {
			Image coinImage = GameObject.Find ("Coll1").GetComponent<Image> ();
			coinImage.color += new Color (0, 0, 0, 1);

		}
		if (coinsArray.Count == 2) {
			Image coinImage = GameObject.Find ("Coll2").GetComponent<Image> ();
			coinImage.color += new Color (0, 0, 0, 1);

		}
		if (coinsArray.Count == 3) {
			Image coinImage = GameObject.Find ("Coll3").GetComponent<Image> ();
			coinImage.color += new Color (0, 0, 0, 1);

		}
		if(GameManager.instance.coinsArray.Count >= 3){
			coins = true;
		}
	}

	public int endLevel(){
		//Implícito que o player já finalizou o level
		GameManager.instance.moedaStuff();
		int gainedPoints = 1;
		if(coins){
			gainedPoints += 1;
		}
		//INSERIR Pontuação por tempo
		if(GameManager.instance.MaxTime >= GameManager.instance.GetComponent<Timer>().time){
			gainedPoints += 1;
		}
		return gainedPoints;
	}
	public void restartCurrentScene(){
		Scene scene = SceneManager.GetActiveScene(); 
		SceneManager.LoadScene(scene.name);

	}
	public void destroyGameManager(){
		Destroy (this.gameObject);
	}
	public void playCollectSFX(){
		SoundManager.instance.PlaySingleSource3 (coinCollect);
	}
	public void playLVLCompleteSFX(){
		SoundManager.instance.PlaySingleSource3 (levelCompleteSound);
	}
	public void playTeleportSFX(){
		SoundManager.instance.PlaySingleSource3 (teleportSound);
	}
	public void resetCoinUI(){
		if (coinsArray.Count >= 1) {
			Image coinImage = GameObject.Find ("Coll1").GetComponent<Image> ();
			coinImage.color = new Color32 (0xFF,0xFF,0xFF,0x3B);
		}
		if (coinsArray.Count >= 2) {
			Image coinImage2 = GameObject.Find ("Coll2").GetComponent<Image> ();
			coinImage2.color = new Color32 (0xFF,0xFF,0xFF,0x3B);
		}
		if (coinsArray.Count >= 3) {
			Image coinImage3 = GameObject.Find ("Coll3").GetComponent<Image> ();
			coinImage3.color = new Color32 (0xFF,0xFF,0xFF,0x3B);
		}
	}
}
