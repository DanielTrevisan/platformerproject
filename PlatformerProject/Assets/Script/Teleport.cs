﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
	
	static Transform last;

	public Vector3 localWaypoint;
	Vector3 globalWaypoint;

	void OnTriggerEnter ( Collider other )
	{
		if ( globalWaypoint == last.position )
			return;
		TeleportToExit( other );

	}
	void OnTriggerExit ( )
	{
		if ( globalWaypoint == last.position )
			last = null;
	}
	void TeleportToExit ( Collider other )
	{
		globalWaypoint = localWaypoint + transform.position;
		last = transform;
		other.transform.position = globalWaypoint;
	}

	public void TeleportToExit2 ( Transform other )
	{
		globalWaypoint = localWaypoint + transform.position;
		last = transform;
		other.position = globalWaypoint;
	}

	public Vector3 GetGlobal(){
		return globalWaypoint;
	}

	public Transform GetLast(){
		return last;
	}

	void OnDrawGizmos(){
		if (localWaypoint != null) {
			Gizmos.color = Color.green;
			float size = .3f;


				Vector3 globalWaypointPos = (Application.isPlaying)?globalWaypoint : localWaypoint + transform.position;
				Gizmos.DrawLine (globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
				Gizmos.DrawLine (globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);


		}
	}
}
