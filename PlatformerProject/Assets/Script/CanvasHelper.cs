﻿using UnityEngine;
using System.Collections;

public class CanvasHelper : MonoBehaviour {

	GameObject playerObject;

	void Start(){
		playerObject = GameObject.FindGameObjectWithTag ("Player");
	}

	// Use this for initialization
	public void openOptions(){
		GameManager.instance.pauseGame ();
		SoundManager.instance.openOption ();
	}

	public void restartLevel(bool FromOptionsMenu){
		Player player = playerObject.GetComponent<Player> ();
		player.ReloadLevel ();
		if (FromOptionsMenu) {
			SoundManager.instance.closeOptions ();
		}
	}

	public void returnLevelSelect(bool FromOptionsMenu){
		LoadOnClick loader = this.gameObject.GetComponent<LoadOnClick> ();
		loader.loadByIndex (1);
		if (FromOptionsMenu) {
			SoundManager.instance.closeOptions ();
		}
		GameManager.instance.destroyGameManager ();
	}

	public void exitOptions(){
		GameManager.instance.unpauseGame ();
		SoundManager.instance.closeOptions ();
	}

	public void changeDirectionPress(){
		GameObject playerObject = GameObject.FindGameObjectWithTag ("Player");
		Player player = playerObject.GetComponent<Player> ();
		player.changeDirection ();
	}

}
