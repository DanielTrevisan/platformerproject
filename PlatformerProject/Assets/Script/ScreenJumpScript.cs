﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class ScreenJumpScript : MonoBehaviour,IPointerUpHandler,IPointerDownHandler {

	// Use this for initialization
	Player player;
	static bool status = true;

	// Use this for initialization
	void Start () {
		GameObject GO = GameObject.FindGameObjectWithTag("Player");
		player = GO.GetComponent<Player> ();
	}

	void Update (){
		print(status);
		if(status){
			player.OnJumpInputUp();
		}else{
			player.OnJumpInputDown();
		}
	}
		
	public void OnPointerDown(PointerEventData eventData){
		

		print("Exit");
		status = false;

	}
	public void OnPointerUp(PointerEventData eventData){

		print("Enter");
		status = true;

	}
}
