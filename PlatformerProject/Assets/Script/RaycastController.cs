﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (BoxCollider2D))]

public class RaycastController : MonoBehaviour {
	
	public LayerMask collisionMask;

	public const float skinWidth = .015f;
	const float dstBtwnRays = .25f;

	[HideInInspector]
	public int horizontalRayCount;
	[HideInInspector]
	public int verticalRayCount;

	[HideInInspector]
	public float horizontalRaySpacing;
	[HideInInspector]
	public float verticalRaySpacing;

	[HideInInspector]
	public BoxCollider2D colider;
	public RaycastOrigins raycastOrigins;

	public virtual void Awake(){
		colider = GetComponent<BoxCollider2D> ();

	}
	public virtual void Start(){
		CalculateRaySpacing ();

	}

	public void UpdateRaycastOrigins(){
		Bounds bounds = colider.bounds;
		bounds.Expand (skinWidth * -2);

		raycastOrigins.bottomLeft = new Vector2 (bounds.min.x, bounds.min.y);
		raycastOrigins.bottomRight = new Vector2 (bounds.max.x, bounds.min.y);
		raycastOrigins.topLeft = new Vector2 (bounds.min.x, bounds.max.y);
		raycastOrigins.topRight = new Vector2 (bounds.max.x, bounds.max.y);

	}

	public void CalculateRaySpacing(){
		Bounds bounds = colider.bounds;
		bounds.Expand (skinWidth * -2);
		float boundsWidth = bounds.size.x;
		float boundsHeight = bounds.size.y;

		horizontalRayCount = Mathf.RoundToInt (boundsHeight / dstBtwnRays);
		verticalRayCount = Mathf.RoundToInt (boundsWidth / dstBtwnRays);


		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
	}


	public struct RaycastOrigins {
		public Vector2 topLeft, topRight, bottomLeft, bottomRight;
	}
}
