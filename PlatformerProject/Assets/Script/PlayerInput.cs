﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent (typeof (Player))]
public class PlayerInput : MonoBehaviour {

	Player player;


	// Use this for initialization
	void Start () {
		player = GetComponent<Player> ();
	}
	
	// Update is called once per frame
	void Update () {
		//Vector2 directionalInput = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
//		if (RunningRight) {
			player.SetDirectionalInput ();
//		} else {
//			player.SetDirectionalInput (new Vector2 (-1, 0));
//		}
		#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
		if (Input.GetKeyDown (KeyCode.Z)) {
			player.OnJumpInputDown ();
		}
		if (Input.GetKeyUp (KeyCode.Z)) {
			player.OnJumpInputUp ();
		}
		#endif
//		#if UNITY_IOS
//		if(Input.touches > 0){
//			player.OnJumpInputDown ();
//		}else{
//			player.OnJumpInputUp ();
//		}
//		#endif
	}
	public void jump(){
		print("aqui");
//		jumpLock = true;
		player.OnJumpInputDown ();
	}
}
