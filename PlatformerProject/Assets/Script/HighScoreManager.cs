﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class HighScoreManager : MonoBehaviour {
	public Sprite filledStar;


	// Use this for initialization
	void Start () {
		setCurrentStars ();

	}

	void setCurrentStars(){
		
		GameObject[] buttons = GameObject.FindGameObjectsWithTag ("LevelButton").OrderBy(go => int.Parse(go.name)).ToArray();

		for (int i = 0; i<buttons.Length; i++) {
			string nameOfLevel = "level" + (i+1) + "Stars";
			float numberofstars = PlayerPrefs.GetInt (nameOfLevel);

			print ("level " + nameOfLevel + ": " + numberofstars + " stars");
				
			if (numberofstars >= 1) {
				Transform star1trans = buttons [i].transform.GetChild (1);
				GameObject star1 = star1trans.gameObject;
				Image starSprite1 = star1.GetComponent<Image> ();
				starSprite1.sprite = filledStar;
				if (numberofstars >= 2) {
					Transform star2trans = buttons [i].transform.GetChild (2);
					GameObject star2 = star2trans.gameObject;
					Image starSprite2 = star2.GetComponent<Image> ();
					starSprite2.sprite = filledStar;
					if (numberofstars >= 3) {
						Transform star3trans = buttons [i].transform.GetChild (3);
						GameObject star3 = star3trans.gameObject;
						Image starSprite3 = star3.GetComponent<Image> ();
						starSprite3.sprite = filledStar;
					}
				}
				}
		}
	}
		// Update is called once per frame
	public void deleteHighScores(){
		PlayerPrefs.DeleteAll ();
	}
}
