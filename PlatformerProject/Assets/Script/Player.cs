﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

	public float maxJumpHeight = 4;
	public float minJumpHeight = 1;
	public float timeToJumpApex = .1f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;

	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;
	public float moveSpeed = 6;

	public Vector2 wallJumpClimb;
	public Vector2 wallJumpOff;
	public Vector2 wallLeap;

	public float wallSlideSpeed = 3;
	public float wallStickTime = .25f;
	float timetoUnstick;

	bool wallSliding;
	int wallDirX;

	public int buttonPress = 1;

	private Vector3 RespawnPosition;

	Controller2D controler;

	Vector2 directionalInput;

	public bool RunningRight = true;
	public bool StopMovement = false;

	public GameObject levelComplete;
	public GameObject particleEmit;
	GameObject panelDefeat;

	public AudioClip jumpingSound;
	public AudioClip steppingSound;
	public AudioClip deathSound;


	public Sprite starCompleted;
	public Sprite rightArrow;
	public Sprite leftArrow;

	[HideInInspector]
	public Animator animator;

	// Use this for initialization
	void Start () {
		

		controler = GetComponent<Controller2D> ();
		RespawnPosition = new Vector3();
		RespawnPosition = this.transform.position;
		gravity = - (2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpHeight);
		print ("Gravity: " + gravity + " Jump Velocity: " + maxJumpVelocity);
		animator = GetComponent<Animator> ();

	}
	void Awake(){
		levelComplete = GameObject.Find ("ImageComplete");
		levelComplete.SetActive (false);

		panelDefeat = GameObject.Find ("PanelEnd");
		panelDefeat.SetActive (false);
	}
	void Update(){
//		#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
		if (StopMovement == false && !GameManager.instance.isSetup()) {
			CalculateVelocity ();
			HandleWallsliding ();
			CheckIfWallsliding ();
			wallDirX = (controler.collisions.left) ? -1 : 1;

			controler.Move (velocity * Time.deltaTime, directionalInput);

			if (controler.collisions.above || controler.collisions.below) {
				if (controler.collisions.slidingDownMaxSlope) {
					velocity.y += controler.collisions.slopeNormal.y * -gravity * Time.deltaTime;
				} else {
					velocity.y = 0;
				}
			}
			if (controler.collisions.below) {
				SoundManager.instance.PlaySingleSource2 (steppingSound);
				animator.SetBool ("OnGround", true);
				animator.SetBool ("IsWallsliding", false);

			}
		}
//		#endif
//		#if UNITY_IOS
//		if (StopMovement == false && !GameManager.instance.isSetup()) {
//			CalculateVelocity ();
//			HandleWallsliding ();
//			wallDirX = (controler.collisions.left) ? -1 : 1;
//
//			controler.Move (velocity * Time.deltaTime, directionalInput);
//
//			if (controler.collisions.above || controler.collisions.below) {
//				if (controler.collisions.slidingDownMaxSlope) {
//					velocity.y += controler.collisions.slopeNormal.y * -gravity * Time.deltaTime;
//				} else {
//					velocity.y = 0;
//				}
//			}
//			if (controler.collisions.below) {
//				SoundManager.instance.PlaySingleSource2 (steppingSound);
//			}
//		}
//		#endif
	}
	public void SetDirectionalInput (){
		if (RunningRight) {
			directionalInput = new Vector2 (1, 0);
		} else {
			directionalInput = new Vector2 (-1, 0);

		}
	}

	public void changeDirection(){
		print("Botao");
		print("change Direction: "+ RunningRight);
		if(wallSliding){
			velocity.x = -wallDirX * wallJumpOff.x;
			velocity.y = wallJumpOff.y;
			animator.SetBool ("EndWallSlide", true);
			animator.SetBool ("IsWallsliding", false);
		}
		GameObject changeDirBut = GameObject.Find ("ChangeDirection");
		if (RunningRight) {
			RunningRight = false;
			changeDirBut.GetComponent<Image> ().sprite = rightArrow;
			SpriteRenderer sprite = this.gameObject.GetComponent<SpriteRenderer> ();

			sprite.flipX = true;

		} else {
			RunningRight = true;
			changeDirBut.GetComponent<Image> ().sprite = leftArrow;
			SpriteRenderer sprite = this.gameObject.GetComponent<SpriteRenderer> ();

			sprite.flipX = false;

		}
	}

	public void OnJumpInputDown(){
		if (wallSliding) {
//			if (wallDirX == directionalInput.x) {
//				velocity.x = -wallDirX * wallJumpClimb.x;
//				velocity.y = wallJumpClimb.y;
//			} else if (directionalInput.x == 0) {
//				velocity.x = -wallDirX * wallJumpOff.x;
//				velocity.y = wallJumpOff.y;
//			} else {
			velocity.x = -wallDirX * wallLeap.x;
			velocity.y = wallLeap.y;
			SoundManager.instance.PlaySingle (jumpingSound);
			animator.SetTrigger("StartJump");
			animator.SetBool ("OnGround", false);
			animator.SetBool ("IsWallsliding", false);
			wallSliding = false;
			changeDirection ();
		}
		if (controler.collisions.below){
			if (controler.collisions.slidingDownMaxSlope) {
				if (directionalInput.x != -Mathf.Sign (controler.collisions.slopeNormal.x)) { //not jumping againnst max slope
					velocity.y = maxJumpVelocity * controler.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controler.collisions.slopeNormal.x;
					animator.SetTrigger("StartJump");
					animator.SetBool ("OnGround", false);

					SoundManager.instance.PlaySingle (jumpingSound);

				}
			} else {
				velocity.y = maxJumpVelocity;
				animator.SetTrigger("StartJump");
				animator.SetBool ("OnGround", false);

				SoundManager.instance.PlaySingle (jumpingSound);

			}
		}
	}

	public void OnJumpInputUp(){
		if (velocity.y > minJumpVelocity) {
			velocity.y = minJumpVelocity;
		}
	}
	

	void HandleWallsliding(){
		wallSliding = false;
		animator.SetBool ("IsWallsliding", false);

		if ((controler.collisions.left || controler.collisions.right) && !controler.collisions.below && velocity.y < 0) {
			wallSliding = true;
			animator.SetBool ("IsWallsliding", true);

			if (velocity.y < -wallSlideSpeed) {
				velocity.y = -wallSlideSpeed;
			}
			if (timetoUnstick > 0) {
				velocityXSmoothing = 0;
				velocity.x = 0;
				if (directionalInput.x != wallDirX && directionalInput.x != 0) {
					timetoUnstick -= Time.deltaTime;
				} else {
					timetoUnstick = wallStickTime;
				}
			} else {
				timetoUnstick = wallStickTime;
			}

		}
	}
	void CalculateVelocity(){
		float targetVelocityX = directionalInput.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controler.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
	}


	public void Dead(){
		//INSERIR ALGUMA ANIMACAO DE MORTE (Estilo Mario?)

		ParticleSystem particles = particleEmit.GetComponent<ParticleSystem>();
		Instantiate (particleEmit, this.transform.position, Quaternion.identity);
		particles.Emit (100);

		SoundManager.instance.PlaySingleSource3 (deathSound);


		GameObject options = GameObject.Find ("ButtonOptions");
		Button butOptions = options.GetComponent<Button> ();
		butOptions.enabled = false;
		this.gameObject.SetActive(false);
		Invoke("RestartLevel", 3);
	}
	private void RestartLevel(){
		panelDefeat.SetActive (true);
	}
	public void ReloadLevel(){
		this.gameObject.SetActive (true);
		this.RunningRight = true;
		this.transform.position = RespawnPosition;
		this.velocity = new Vector3(0,0,0);

		//Destroy (particleEmit);
		GameObject changeDirBut = GameObject.Find ("ChangeDirection");
		changeDirBut.GetComponent<Image> ().sprite = leftArrow;
		SpriteRenderer sprite = this.gameObject.GetComponent<SpriteRenderer> ();
		sprite.flipX = false;


		GameObject[] arrayPlatforms =  GameObject.FindGameObjectsWithTag("MovingPlat");
		foreach (GameObject platform in arrayPlatforms) {
			platform.GetComponent<PlatformerController> ().ResetPlatform ();
		}
		GameObject[] arrayMovingDeath =  GameObject.FindGameObjectsWithTag("DeathZone");
		foreach (GameObject deathZone in arrayMovingDeath) {
			if( deathZone.GetComponent<PlatformerController>())
			deathZone.GetComponent<PlatformerController> ().ResetPlatform ();
		}
		foreach(GameObject coin in GameManager.instance.coinsArray){
			coin.SetActive(true);
		}
		GameManager.instance.resetCoinUI();
		GameManager.instance.coinsArray.Clear();

		Timer timer = GameManager.instance.GetComponent<Timer> ();
		timer.resetTimer ();
		timer.startTimer ();
		GameObject cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
		CameraFollow cameraScript = cameraObject.GetComponent<CameraFollow> ();
		cameraScript.StopCamera = false;

		GameObject options = GameObject.Find ("ButtonOptions");
		Button butOptions = options.GetComponent<Button> ();
		butOptions.enabled = true;

		StopMovement = false;
	}
	public void LevelComplete(){
		//INSERIR ANIMACAO DE LEVEL COMPLETE
		StopMovement = true;
		levelComplete.SetActive (true);

		Timer timer = GameManager.instance.GetComponent<Timer> ();
		timer.stopCounting ();
		print("Pontuação: " + GameManager.instance.endLevel());

		if (GameManager.instance.endLevel () >= 2) {
			GameObject star2 = GameObject.Find ("Star2");
			Image starSprite2 = star2.GetComponent<Image> ();
			starSprite2.sprite = starCompleted;
			if (GameManager.instance.endLevel () >= 3) {
				GameObject star3 = GameObject.Find ("Star3");
				Image starSprite3 = star3.GetComponent<Image> ();
				starSprite3.sprite = starCompleted;
			}
		}
		string nameOfLevel = "level"+GameManager.instance.Level();
		print ("nameofLevel: "+ nameOfLevel);
		if(PlayerPrefs.GetInt(nameOfLevel+"Coins") < GameManager.instance.coinsArray.Count){
			PlayerPrefs.SetInt(nameOfLevel+"Coins",GameManager.instance.coinsArray.Count);
		}
		if(PlayerPrefs.GetInt(nameOfLevel+"Stars") < GameManager.instance.endLevel() || PlayerPrefs.GetInt(nameOfLevel+"Stars") == 0){
			PlayerPrefs.SetInt(nameOfLevel+"Stars",GameManager.instance.endLevel());
		}
		if(PlayerPrefs.GetFloat(nameOfLevel+"Time") > GameManager.instance.GetComponent<Timer>().time || PlayerPrefs.GetFloat(nameOfLevel+"Time") == 0f){
			PlayerPrefs.SetFloat(nameOfLevel+"Time",GameManager.instance.GetComponent<Timer>().time);
		}
		print("Moedas: " + PlayerPrefs.GetInt(nameOfLevel+"Coins"));
		print("Tempo: " + PlayerPrefs.GetFloat(nameOfLevel+"Time"));
		print("Stars: " + PlayerPrefs.GetInt(nameOfLevel+"Stars"));
	}
	private void CheckIfWallsliding(){
		if (!wallSliding && (!controler.collisions.below)) {
			animator.SetBool ("EndWallSlide", true);
		} else if (wallSliding) {
			animator.SetBool ("EndWallSlide", false);
			animator.SetBool ("OnGround", false); 
		}

	}
}

